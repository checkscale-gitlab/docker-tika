# Apache Tika Server Docker Image

## References

*	[tika-docker from upstream](https://github.com/apache/tika-docker) -- Debian-based
*	[Introduction to Tika server](https://cwiki.apache.org/confluence/display/TIKA/TikaServer)

## Build

### The Latest Version by Docker

~~~sh
docker build --pull -t "registry.gitlab.com/rychly-edu/docker/docker-tika:latest" .
~~~

### All Versions by the Build Script

~~~sh
./build.sh --build "registry.gitlab.com/rychly-edu/docker/docker-tika" "latest"
~~~

For the list of versions to build see [docker-tags.txt file](docker-tags.txt).
