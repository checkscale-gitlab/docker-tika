#!/bin/sh

if [ -n "${PDF_IMAGES}" ]; then
	sed -i "s|\\(<param name=\"extractInlineImages\" type=\"bool\">\\)[^<]*\(</param>\\)|\\1${PDF_IMAGES:-true}\\2|" /tika-config.xml
fi

if [ -n "${OCR_LANGUAGES}" ]; then
	sed -i "s|\\(<param name=\"language\" type=\"string\">\\)[^<]*\(</param>\\)|\\1${OCR_LANGUAGES:-ces+deu+fra+ita+pol+slk+spa}\\2|" /tika-config.xml
fi

if [ $# -gt 0 ]; then
	# custom
	exec $@
else
	# default
	exec java \
		-classpath "${TIKA_HOME}/jai-imageio-core.jar:${TIKA_HOME}/jai-imageio-jpeg2000.jar:${TIKA_HOME}/sqlite-jdbc.jar:${TIKA_HOME}/tika-server.jar" \
		org.apache.tika.server.TikaServerCli \
		--config /tika-config.xml \
		--host '*' \
		--log debug \
		--port ${TIKA_PORT:-9998} \
		--includeStack \
		-spawnChild
fi
