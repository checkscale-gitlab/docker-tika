#!/bin/sh

# https://cwiki.apache.org/confluence/display/TIKA/TikaServer#TikaServer-MetadataResource

FILE_PATH="${1}"
FILE_NAME="${FILE_PATH##*/}"
FILE_TYPE=$(file --dereference --brief --mime-type "${FILE_PATH}")

URL="http://localhost:9998/meta"

OUT="${2}"
if [ -z "${OUT}" ]; then
	# XMP is a standard, https://en.wikipedia.org/wiki/Extensible_Metadata_Platform
	OUT="application/rdf+xml"
	#OUT="application/json"
	#OUT="text/csv"
fi

exec curl -i -X PUT -H "Accept: ${OUT}" --data-binary "@${FILE_PATH}" -H "Content-Disposition: attachment; filename=${FILE_NAME}" -H "Content-Type: ${FILE_TYPE}" "${URL}"
